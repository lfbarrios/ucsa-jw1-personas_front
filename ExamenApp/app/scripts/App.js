var examenApp = angular.module('examenApp', [ 'ui.router' ]);

examenApp.constant('Config', {
	"backendURL" : "http://localhost:8090/examen.api"
});

examenApp.config(function($stateProvider) {
	$stateProvider.state('personas-list', {
		url : '/personas',
		templateUrl : '../partials/lista-personas.html',
		controller : 'PersonasListCtrl'
	});
	$stateProvider.state('personas-view', {
		url : '/personas/:id/ver',
		templateUrl : '../partials/ver-persona.html',
		controller : 'PersonaViewCtrl'
	}).state('personas-new', {
		url : '/personas/nuevo',
		templateUrl : '../partials/nuevo-persona.html',
		controller : 'PersonaNuevoCtrl'
	}).state('personas-edit', {
		url : '/personas/:id/editar',
		templateUrl : '../partials/editar-persona.html',
		controller : 'PersonaEditCtrl'
	});
}).run(function($state) {
	$state.go('personas-list');
});
