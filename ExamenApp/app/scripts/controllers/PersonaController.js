'use strict'
examenApp
		.controller(
				'PersonasListCtrl',
				[
						'$scope',
						'$http',
						'PersonaService',
						'$state',
						'$window',
						function($scope, $http, PersonaService, $state, $window) {
							$scope.persona = {};
							// AL CREARSE EL CONTROLLER PRINCIPAL, SE EJECUTARA
							// ESTA
							// PETICION
							// QUE OBTENDRA DEL BACKEND LA LISTA DE PERSONAS Y
							// LA
							// ASIGNARA A UNA
							// VARIABLE DENTRO DEL $scope LLAMADA listaPersonas,
							// PARA
							// LUEGO
							// SER MOSTRADA EN LA VISTA MEDIANTE TWO-WAY
							// DATABINDING
							$scope.obtenerPersonas = function() {
								console.log(PersonaService);
								PersonaService.obtenerPersonas().success(
										function(x) {
											$scope.listaPersonas = x;
											console.log(x);
										}).error(function(data) {
									console.log(data);
								});
							};
							$scope.obtenerPersonas();
							$scope.eliminarPersona = function(id) {
								PersonaService.eliminarPersona(id).success(
										function(data) {
											$scope.obtenerPersonas();
											console.log(data);
										}).error(function(data) {
									console.log('OCURRIO UN ERROR');
									console.log(data);
								});
								$window.location.href = '';
							};
						} ])
		.controller(
				'PersonaViewCtrl',
						function($scope, $stateParams, PersonaService) {
							$scope.obtenerPersonaPorId = function () {
								PersonaService.obtenerPersonasId($stateParams.id)
								.success(function(x) {
									$scope.persona = x;
									console.log(x);
								})
								.error(function(data){
									console.log(data);
								});
							};
							$scope.obtenerPersonaPorId($stateParams.id);
						})
		.controller(
				'PersonaNuevoCtrl',
				[
						'$scope',
						'$state',
						'$stateParams',
						'PersonaService',
						'$http',
						function($scope, $state, $stateParams, PersonaService,
								$http) {// a
							$scope.persona = {};
							// CREATE
							// =================================================================
							// Cuando se añade una nueva Persona, manda los
							// datos a la API
							$scope.crearPersona = function() {
								console.log($.isEmptyObject($scope.persona));
								// Validamos el objeto persona para asegurarnos
								// de que haya algo
								// en él
								// Si el form está vacío, no pasa nada
								// El usuario no podrá mantener presionada la
								// tecla enter para
								// ir
								// agregando el mismo registro
								if (!$.isEmptyObject($scope.persona)) {
									PersonaService
											.crearPersona($scope.persona)
											// INVOCAMOS AL
											// BACKEND
											// SOLICITANDO LA INSERCION
											// DEL OBJETO $scope.persona
											.success(
													function(data) {// SI LA
														// PETICION
														// FUE
														// PROCESADA
														// CON
														// EXITO:
														$scope.persona = {};// LIMPIAMOS
														// LA
														// VARIABLE
														// $scope.persona, PARA
														// PODER CARGAR OTRA
														// PERSONA
														// SI MI SERVICIO REST,
														// UNA VEZ INSERTADO EL
														// REGISTRO,
														// ME DEVUELVE
														// LA LISTA ACTUALIZADA,
														// LA ASIGNO A LA 
														// VARIABLE
														// $scope.listaPersonas
														// COMO SE VE EN LA
														// SIGUIENTE LINEA
														// $scope.listaPersonas
														// = data; 
														// PERO COMO MI BACKEND
														// NO ME DEVUELVE UNA
														// LISTA, SINO
														// LA URL DEL
														// OBJETO CREADO (201), 
														// ENTONCES, DEBO
														// REALIZAR OTRA
														// PETICION PARA OBTENER
														// LA
														// LISTA
														// ACTUALIZADA
														$scope.obtenerPersonas = function() {
															console
																	.log(PersonaService);
															PersonaService
																	.obtenerPersonas()
																	.success(
																			function(
																					x) {
																				$scope.listaPersonas = x;
																				console
																						.log(x);
																			})
																	.error(
																			function(
																					data) {
																				console
																						.log(data);
																			});
														};
														$scope
																.obtenerPersonas();
														// IMPRIMO LOS DATOS
														// ENVIADOS DESDE EL
														// BACKEND
														console.log(data);

														$state
																.go('personas-list');
													}).error(function(data) {
												console.log(data);
											});
									// , function(data) {
									// console.log(data);
									// }
								}

							};// fin de crearPersona()
						}// a
				])// Acá termina el controller para crear persona
		.controller(
				'PersonaEditCtrl',
				function($scope, $state, $stateParams, PersonaService) {

					// UPDATE
					// =================================================================
					// Cuando se modifica una Persona, envia los datos a la API
					$scope.modificarPersona = function() {
						// Validamos el objeto persona para asegurarnos de que
						// haya algo en él
						// Si el form está vacío, no pasa nada
						// El usuario no podrá mantener presionada la tecla
						// enter para ir enviando el mismo registro
					
							PersonaService.modificarPersona($scope.persona)
									.success(function(data) {
										$scope.persona = {};

										$state.go('personas-list');
									}).error(function(data) {
										console.log('OCURRIO UN ERROR');
										console.log(data);
									});

						

					}
				});