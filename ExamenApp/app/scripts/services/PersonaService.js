'use strict'
examenApp
	.factory('PersonaService', ['$http', 'Config', function($http, Config){
		return {
		
			obtenerPersonas : function(){
				return $http.get(Config.backendURL + '/persona/');
			},
			obtenerPersonasId : function(id){
				return $http.get(Config.backendURL + '/persona/id/' + id);
			},
			crearPersona : function(persona){
				return $http.post(Config.backendURL + '/persona/', persona);
			},
			modificarPersona : function(persona){
				return $http.put(Config.backendURL + '/persona/', persona);
			},
			eliminarPersona : function(id){
				return $http.delete(Config.backendURL + '/persona/' + id);
			}
		};
	
	}]);